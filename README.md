<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Reto Técnico - Backend developer

El reto consiste en utilizar el framework Laravel para replicar la funcionalidad de esta api
(https://jobs.backbonesystems.io/api/zip-codes/01210), utilizando esta fuente de información.
El tiempo de respuesta promedio debe ser menor a 300 ms, pero entre menor sea, mejor.
[GET] https://jobs.backbonesystems.io/api/zip-codes/{zip_code}


## Solución

- 1. Modelamiento de la base de datos en MySql.
- 2. Se importa la data de los archivos de excel a la base de datos.
- 3. Se crea endpoint solicitado: /api/zip-codes/{zip_code}.
