<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ZipCode;

class ZipCodeController extends Controller
{

    public function getZipCode($zip_code){

        $zipCodes = ZipCode::where("d_codigo", $zip_code)->get();

        if(count($zipCodes) == 0){

            $returnData = array(
                'status' => 'ERROR',
                'message' => 'Record not found',
            );

            $statusData = 404;

        }else{

            $data = $zipCodes->first();

            //federal entity --
            $federal_entity = new \stdClass();
            $federal_entity->key  = $data->c_estado;
            $federal_entity->name = mb_strtoupper($this->stripAccents($data->d_estado));
            $federal_entity->code = $data->c_cp;

            //settlements --
            $settlements = collect();
            foreach ($zipCodes as $zipCode){

                $settlement_type = new \stdClass();
                $settlement_type->name = $zipCode->d_tipo_asenta;

                $settlem = new \stdClass();
                $settlem->key                = $zipCode->id_asenta_cpcons;
                $settlem->name               = mb_strtoupper($this->stripAccents($zipCode->d_asenta));
                $settlem->zone_type          = mb_strtoupper($zipCode->d_zona);
                $settlem->settlement_type    = $settlement_type;

                $settlements->push($settlem);

            }

            //municipality --
            $municipality = new \stdClass();
            $municipality->key  = $data->c_mnpio;
            $municipality->name = mb_strtoupper($this->stripAccents($data->d_mnpio));

            //result --
            $returnData = new \stdClass();
            $returnData->zip_code = $data->d_codigo;
            $returnData->locality = mb_strtoupper($this->stripAccents($data->d_ciudad));
            $returnData->federal_entity = $federal_entity;
            $returnData->settlements  = $settlements;
            $returnData->municipality = $municipality;

            $statusData = 200;

        }

        return response()->json($returnData, $statusData);

    }

    function stripAccents($str) {
        return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

}
