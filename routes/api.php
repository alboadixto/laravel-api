<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get("zip-codes/{zip_code}", [\App\Http\Controllers\API\ZipCodeController::class, 'getZipCode'])->name("zip-codes");
